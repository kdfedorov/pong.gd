extends Area2D

# Ball's speed
@export var speed := 30.0

# Game screen size
@onready var screen_size := get_viewport_rect().size
# Ball's sprite
@onready var sprite := $Sprite
# Ball's collision
@onready var collision := $CollisionShape
# Size of the ball
@onready var size: Vector2 = sprite.texture.get_size() : set = set_size

# Ball's velocity
var velocity: Vector2 : set = set_velocity
# 
const BOUNCING_ANGLE : float = PI / 8

func launch(initial_position: Vector2, initial_velocity: Vector2):
   position = initial_position
   velocity = initial_velocity.normalized()

# Sets new size to paddle and it's elements
func set_size(new_size: Vector2):
   sprite.apply_scale(new_size / sprite.texture.get_size())
   collision.shape.size = new_size
   size = new_size

func set_velocity(new_velocity: Vector2):
   velocity = new_velocity
   sprite.flip_h = velocity.x < 0
   sprite.flip_v = velocity.y > 0

func _process(delta: float):
   position += speed * velocity * delta
   if position.y < 0 or position.y >= screen_size.y:
      $HitSound.play()
      velocity.y *= -1
      
func _on_ball_collided(paddle: Paddle):
   var half_height = 0.5 * paddle.size.y
   var dist_to_center = paddle.position.y - position.y
   var mult = clamp(dist_to_center / half_height, -1.0, 1.0)
   var angle = sign(velocity.x) * mult * BOUNCING_ANGLE
   print(mult)
   #print("%s = %s + %s * %s" % [rad_to_deg(angle), rad_to_deg(PI), mult, rad_to_deg(BOUNCING_ANGLE)])
   velocity.x *= -1
   velocity = velocity.rotated(angle)
   $HitSound.play()
