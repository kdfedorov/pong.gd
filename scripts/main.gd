extends Node

# Maximum points amount required to win
@export var max_score: int = 10

# Game screen size
@onready var screen_size := get_viewport().get_visible_rect().size
# Player 1 Paddle
@onready var player_one := $GameObjects/PlayerOne
# Player 2 Paddle
@onready var player_two := $GameObjects/PlayerTwo
# Game's ball
@onready var ball := $GameObjects/Ball

# Player 1 Score
@onready var player_one_score = $UI/PlayerOneScore
# Player 2 Score
@onready var player_two_score = $UI/PlayerTwoScore

# Size which were assets designed for
const DESIGN_SIZE := Vector2(192, 108)
# Ratio between initial screen size and game design size
@onready var target_ratio := screen_size / DESIGN_SIZE
# Current score
var score := { "p1": 0, "p2": 0 }

func update_player_one_score(score_value: int):
   score["p1"] = score_value
   player_one_score.text = str(score_value)
   
func update_player_two_score(score_value: int):
   score["p2"] = score_value
   player_two_score.text = str(score_value)

func reset(ball_initial_velocity: Vector2):
   var paddle_margin = player_one.size.x / 2.0 + 5 * target_ratio.x
   player_one.position = Vector2(paddle_margin, screen_size.y / 2.0)
   player_two.position = Vector2(screen_size.x - paddle_margin, screen_size.y / 2.0)
   ball.launch(screen_size / 2.0, ball_initial_velocity)   

func game_process(processed: bool):
   player_one.set_process(processed)
   player_two.set_process(processed)
   ball.set_process(processed)
   
func _ready():
   var paddle_size = player_one.size * target_ratio
   player_one.size = paddle_size
   player_one.speed *= target_ratio.y
   
   player_two.size = paddle_size
   player_two.speed *= target_ratio.y
   
   ball.size *= target_ratio * 0.5
   ball.speed *= target_ratio.length()
   
   Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
   
   reset(Vector2.RIGHT)

func _process(_delta: float):
   if ball.position.x < 0.0 or ball.position.x > screen_size.x:
      game_process(false)
      var initial_velocity := Vector2.ZERO
      if ball.position.x > screen_size.x:
         update_player_one_score(score["p1"] + 1)
         initial_velocity = Vector2.LEFT
      elif ball.position.x < 0:
         update_player_two_score(score["p2"] + 1)
         initial_velocity = Vector2.RIGHT

      reset(initial_velocity)
      await get_tree().create_timer(1.0).timeout
      game_process(true)
