class_name Paddle
extends Area2D

# Paddle vertical speed in pixels per second.
@export var speed := 86
# Input action for moving paddle up
@export var move_up_action := ""
# Input action for moving paddle down
@export var move_down_action := ""

# Game screen size
@onready var screen_size := get_viewport_rect().size
# Paddle's sprite
@onready var sprite := $Sprite
# Paddle's collision shape
@onready var collision := $CollisionShape

# Size of the paddle
@onready var size: Vector2 = sprite.texture.get_size() : set = set_size

# Sets new size to paddle and it's elements
func set_size(new_size: Vector2):
   sprite.apply_scale(new_size / sprite.texture.get_size())
   collision.shape.size = new_size
   size = new_size

func _process(delta: float):
   var direction = Input.get_action_strength(move_down_action) - Input.get_action_strength(move_up_action)
   position.y = clamp(position.y + direction * speed * delta, size.y / 2, screen_size.y - size.y / 2)
